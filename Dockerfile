FROM mariadb:10.7.4

COPY ./mysql /var/lib/mysql 
COPY ./dump /docker-entrypoint-initdb.d
COPY ./docker-assets/mysql/custom.cnf /etc/mysql/conf.d/custom.cnf



